package com.cy.store.service.impl;

import com.cy.store.entity.User;
import com.cy.store.mapper.UserMapper;
import com.cy.store.service.ex.ServiceException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
//用户模块业务层的实现类

@SpringBootTest
@RunWith(SpringRunner.class)
public class UserServiceTests {
    @Autowired
    private UserServiceImpl userService;

    @Test
    public void reg() {
        try {
            User user = new User();
            user.setUsername("张晓伟");
            user.setPassword("123456");
            userService.reg(user);
            System.out.println("OK");
        } catch (ServiceException e) {
            System.out.println(e.getClass().getSimpleName());
            System.out.println(e.getMessage());
        }
    }

    @Test
    public void login() {
        try {
            String username = "张晓伟";
            String password = "123456";
            User user = userService.login(username, password);
            System.out.println("登录成功！" + user);
        } catch (ServiceException e) {
            System.out.println("登录失败！" + e.getClass().getSimpleName());
            System.out.println(e.getMessage());
        }

    }

    @Test
    public void changePasswordTest() {
        userService.changePassword(17, "张晓伟", "123456*", "123456");
    }

    @Test
    public void getByUid() {
        System.out.println(userService.getByUid(19));

    }

    @Test
    public void changeInfo() {
        User user = new User();
        user.setPhone("1232352234");
        user.setEmail("xingfen@qq.com");
        user.setGender(0);
        userService.changeInfo(19, "农夫山泉", user);
    }

    @Test
    public void changeAvatar() {
        userService.changeAvatar(19, "image", "admin");
    }
}
