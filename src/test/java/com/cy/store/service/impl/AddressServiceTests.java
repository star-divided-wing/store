package com.cy.store.service.impl;

import com.cy.store.entity.Address;
import com.cy.store.mapper.AddressMapper;
import com.cy.store.service.IAddressService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)

public class AddressServiceTests {
    @Autowired
    private IAddressService addressService;
    @Test
    public void addNewAddressTest(){
        Address address=new Address();
        address.setPhone("12986377110");
        addressService.addNewAddress(20,address,"admin");
    }
}
