package com.cy.store.mapper;

import com.cy.store.entity.District;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@SpringBootTest
//@RunWith(SpringRunner.class):表示启动单元测试类需要传递一个参数，必须是springRunner的示例类型
@RunWith(SpringRunner.class)
public class DistrictMapperTests {
    @Autowired
    private DistrictMapper districtMapper;
    @Test
    public void FindByParent(){
        List<District> list=districtMapper.findByParent("110100");
        for (District d : list) {
            System.out.println(d);
        }
    }

    @Test
    public void findNameByCode(){
        String name = districtMapper.findNameByCode("420600");
        System.out.println(name);
    }
}
