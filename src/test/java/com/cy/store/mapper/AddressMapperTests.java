package com.cy.store.mapper;

import com.cy.store.entity.Address;
import com.cy.store.entity.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

//@SpringBootTest:表示当前的类是一个测试类，不会随同项目一块打包
@SpringBootTest
//@RunWith(SpringRunner.class):表示启动单元测试类需要传递一个参数，必须是springRunner的示例类型
@RunWith(SpringRunner.class)
public class AddressMapperTests {
    @Autowired
    private AddressMapper addressMapper;
    @Test
    public void insertTest(){
        Address address=new Address();
        address.setUid(20);
        address.setPhone("13986377619");
        address.setAddress("成都");
       addressMapper.insert(address);

    }
    @Test
    public void countByUid(){
        Integer count = addressMapper.countByUid(20);
        System.out.println(count);
    }
}

