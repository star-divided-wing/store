package com.cy.store.mapper;

import com.cy.store.entity.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

//@SpringBootTest:表示当前的类是一个测试类，不会随同项目一块打包
@SpringBootTest
//@RunWith(SpringRunner.class):表示启动单元测试类需要传递一个参数，必须是springRunner的示例类型
@RunWith(SpringRunner.class)
public class UserMapperTests {
    //idea有检测的功能,接口是不能直接创建bean的，（动态代理技术来解决）
    @Autowired
    private UserMapper userMapper;

    /*单元测试方法：就可以单独运行，不用启动整个项目，可以做单元测试，提升了代码段测试效率
     * 1.必须是@Test修饰
     * 2.返回类型必须是void
     * 3.方法的参数列表不指定任何类型
     * 4.方法的访问修饰符必须是public*/
    @Test
    public void insert() {
        User user = new User();
        user.setUsername("Tom");
        user.setPassword("123");
        Integer rows = userMapper.insert(user);
        System.out.println(rows);
    }

    @Test
    public void findByUsername() {
        String username = "user01";
        User result = userMapper.findByUsername(username);
        System.out.println(result);
    }

    @Test
    public void updatePasswordByUid() {
        userMapper.updatePasswordByUid(18, "123456", "界徐盛", new Date());

    }

    @Test
    public void findByUid() {
        System.out.println(userMapper.findByUid(18));
    }

    @Test
    public void updateInfoByUid() {
        User user = new User();
        user.setUid(19);
        user.setPhone("12345678901");
        user.setEmail("test001@qq.com");
        user.setGender(1);
        userMapper.updateInfoByUid(user);
    }

    @Test
    public void updateAvatarTest(){
        userMapper.updateAvatarByUid(19,"image","管理员",new Date());
    }



}

