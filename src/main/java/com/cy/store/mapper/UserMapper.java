package com.cy.store.mapper;

import com.cy.store.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;


//用户模块的持久层接口
public interface UserMapper {
    Integer insert(User user);

    User findByUsername(String username);
    //根据用户的uid来修改用户密码
    /*
    * @param uid 用户的id
    * @param password 用户输入的新密码
    * @param modifiedUser 表示修改的执行者
    * @param modifiedTime 表示修改数据的时间
    * */
    Integer updatePasswordByUid( @Param("uid") Integer uid,
                                 @Param("password") String password,
                                 @Param("modifiedUser") String modifiedUser,
                                 @Param("modifiedTime") Date modifiedTime);
    //根据用户id查询用户的数据
    User findByUid(@Param("uid") Integer uid);
    /*
    *更新用户数据信息
    * @Param user 用户的数据
    * @return 返回值为受影响的行数
     */
    Integer updateInfoByUid(User user);
    //根据uid修改用户头像
    /*@Param("SQL映射文件中#{}占位符的变量名)
    * */
    Integer updateAvatarByUid(@Param("uid") Integer uid,
                              @Param("avatar") String avatar,
                              @Param("modifiedUser") String modifiedUser,
                              @Param("modifiedTime") Date modifiedTime);

    
}
