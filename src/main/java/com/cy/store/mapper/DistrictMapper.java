package com.cy.store.mapper;

import com.cy.store.entity.District;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
@Mapper
public interface DistrictMapper {
    /**
     * 根据父代号查询区域信息
     * @param parent 父代号
     * @return 某个父区域的所有区域列表
     */
    List<District> findByParent(String parent);

    /**
     * 根据编码找省市区
     * @param code
     * @return
     */
    String findNameByCode(String code);
}
