package com.cy.store.interceptor;

import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//定义一个拦截器
public class loginInterceptor implements HandlerInterceptor {
    //检测全局session对象中是否有uid数据,如果没有重定向到登录页面
    /*@param request 请求对象
    *@param response 响应对象
    *@param handler 处理器(url+Controller:映射)
    *@return 如果返回值为TRUE表示放行当前的请求，如果返回值是FALSE则表示拦截当前请求*/
    @Override
    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response,
                             Object handler) throws Exception {
        Object obj = request.getSession().getAttribute("uid");
        if(obj==null){//说明用户没有登陆过系统，重定向
            response.sendRedirect("/web/login.html");
            //结束后续调用
            return false;
        }
        //请求放行
        return true;
    }
}
