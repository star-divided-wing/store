package com.cy.store.controller;

import com.cy.store.controller.ex.*;
import com.cy.store.service.ex.*;
import com.cy.store.util.JsonResult;
import org.springframework.boot.web.servlet.server.Session;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.HandlerExceptionResolver;

import javax.servlet.http.HttpSession;
import javax.sql.rowset.serial.SerialException;

//控制层类的基类
public class BaseController {
    //操作成功状态码
    public static final int OK=200;
    //请求处理方法，这个方法的返回值就是需要传递给前端的数据
    //自动将异常传递给此方法的参数列表上
    //当项目中产生了异常，被同意拦截测方法中，这个方法就充当的是请求处理方法，方法的返回值就直接给到前端
    @ExceptionHandler({SerialException.class,FileUploadException.class})//用于统一处理抛出的异常
    public JsonResult<Void> handleException(Throwable e){
        JsonResult<Void> result=new JsonResult<>();
        if(e instanceof UsernameDuplicateException){
            result.setState(4000);
            result.setMessage("用户名已经被占用");
        }else if (e instanceof UserNotFoundException) {
            result.setState(4001);
            result.setMessage("用户数据不存在的异常");
        }else if (e instanceof PasswordNotMatchException) {
            result.setState(4002);
            result.setMessage("用户名密码错误的异常");
        } else if (e instanceof AddressCountLimitException) {
            result.setState(4003);
            result.setMessage("用户的收货地址超出上限的异常");
        } else if (e instanceof InsertException) {
            result.setState(5000);
            result.setMessage("注册时产生未知的异常");
        } else if (e instanceof UpdateException) {
            result.setState(5001);
            result.setMessage("更新数据时产生未知的异常");
        } else if (e instanceof FileEmptyException) {
            result.setState(6000);
            result.setMessage("文件为空！！！");
        } else if (e instanceof FileSizeException) {
            result.setState(6001);
            result.setMessage("文件大小超出限制");
        } else if (e instanceof FileTypeException) {
            result.setState(6002);
            result.setMessage("文件格式错误");
        } else if (e instanceof FileStateException) {
            result.setState(6003);
            result.setMessage("文件状态异常");
        } else if (e instanceof FileUploadIOException) {
            result.setState(6004);
            result.setMessage("文件读写异常");
        }
        return result;
    }
    //获取session对象中的uid,
    protected final Integer getUidFromSession(HttpSession session){
        return Integer.valueOf(session.getAttribute("uid").toString());
    }
    //获取当前用户的Username
    protected final String getUsernameFromSession(HttpSession session){
         return session.getAttribute("username").toString();
    }




}
