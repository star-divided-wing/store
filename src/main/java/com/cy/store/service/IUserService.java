package com.cy.store.service;

import com.cy.store.entity.User;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

//用户模块业务层接口
@Service
public interface IUserService {
    //用户注册方法
//    @Param user 用户的数据对象
    void reg(User user);
//用户登录功能
    //当前匹配的用户数据，如果没有则返回null
    User login(String username, String password);

    void changePassword(Integer uid,
                        String username,
                        String oldPassword,
                        String newPassword);
    //通过id获得用户数据
    User getByUid(Integer uid);
    /*
    * 更新用户数据的操作*/
    void changeInfo(Integer uid,String username,User user);

    /**
     * 修改用户的头像
     * @param uid 用户的id
     * @param avatar 用户的头像路径
     * @param username 用户的名称
     */
    void changeAvatar(Integer uid,String avatar,String username);
}
