package com.cy.store.service;

import com.cy.store.entity.Address;
import org.springframework.stereotype.Service;

/**
 * 收货地址业务层接口
 */
@Service
public interface IAddressService {
    /**
     *创建新的地址
     * @param uid
     * @param address
     * @param username
     */
    void addNewAddress(Integer uid,Address address,String username);
}
